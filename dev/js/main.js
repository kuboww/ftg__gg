$(document).ready(function(){
//fancybox
    $("[data-fancybox]").fancybox({
		// Options will go here
	});
    // nav toggle
        $('.jsToggleMenu').click(function(){
            $(this).find('#nav').slideToggle('slow');

        });
//ajax
(function ($) {

    'use strict';

    $(document).ready(function () {

        // Init here.
        var $body = $('body'),
            $main = $('#main'),
            $site = $('html, body'),
            transition = 'fade',
            smoothState;

        smoothState = $main.smoothState({
            onBefore: function($anchor, $container) {
                var current = $('[data-viewport]').first().data('viewport'),
                    target = $anchor.data('target');
                current = current ? current : 0;
                target = target ? target : 0;
                if (current === target) {
                    transition = 'fade';
                } else if (current < target) {
                    transition = 'moveright';
                } else {
                    transition = 'moveleft';
                }
            },
            onStart: {
                duration: 400,
                render: function (url, $container) {
                    $main.attr('data-transition', transition);
                    $main.addClass('is-exiting');
                    $site.animate({scrollTop: 0});
                }
            },
            onReady: {
                duration: 0,
                render: function ($container, $newContent) {
                    $container.html($newContent);
                    $container.removeClass('is-exiting');
                }
            },
        }).data('smoothState');

    });

}(jQuery));




// checkbox slide box toggle
    $('.jsToggleSlideBox').click(function(){
        $(this).next('.checkbox-slide-block').slideToggle('slow');
        // $(this).next('.checkbox-slide-block').find('input').val(""); // clear input

    });

    //smoothscroll

    $("#inner-nav a").on('click', function(event) {
   if (this.hash !== "") {
     event.preventDefault();
     var hash = this.hash;
     $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 800, function(){
       window.location.hash = hash;
     });
   }
 });






});
